#! /usr/bin/env python3
import requests
import re
import logging

ip_url = ['https://api.ipify.org', 'https://myexternalip.com/raw', 'https://ifconfig.co/ip']

class PublicIpGetter():
    '''
    providesFile must be the path to a file containing a list ip provider seperated by 
    new line
    '''
    def __init__(self, providersFile: str):
        logging.basicConfig(filename = '/tmp/public-getter-service.log')
        try:
            urls_data = open(providersFile, 'r').read()
            self.urls = self._get_urls_from_file(urls_data)
        except IOError:
            logging.info('Couldn\'t open file')

    def get(self):
        for url in self.urls:
            response = requests.get(url)
            if response.status_code == 200:
                ip = self._get_ip(response.content.decode('utf-8'))
                return ip
        logging.info('Couldn\'t get a public ip from any of the listed provider')

    def _get_ip(self, content: str):
        valid_ip = r"^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$"
        return re.search(valid_ip, content)[0]

    def _get_urls_from_file(self, urls_data: str):
        urls = []
        if urls_data:
            urls_str = urls_data.split('\n')
            for line in urls_str:
                if line != '':
                    urls.append(line)
            return urls
        else:
            return None
    
if __name__ == '__main__':
    pig = PublicIpGetter('ip-providers.txt')
    print(pig.get())
